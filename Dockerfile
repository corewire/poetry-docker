### Base Python Image with Poetry ###

FROM python:3.10.8-slim-bullseye

LABEL author="team@corewire.de"

# Get ENV from outside
ARG POETRY_VERSION
ENV POETRY_VERSION=$POETRY_VERSION

# install poetry
RUN pip install --no-cache-dir poetry==$POETRY_VERSION
